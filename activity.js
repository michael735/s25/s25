
//No. 2

db.fruits.aggregate([
    {
        $match: { "onSale": true}
    },
    {
        $count: "fruitOnSale"
    }
])

// No.3

db.fruits.aggregate([
    {
        $match: { "stock": {$gte: 20}}
    },
    {
        $count: "enoughStock"
    }
])

// No. 4

db.fruits.aggregate([
    {
        $match: { "onSale": true}
    },
    {
        $group: {_id: null, avg_price: { $avg: "$price" }}
    }
    
])



// No. 5

db.fruits.aggregate([
    {
        $match: { "onSale": true}
    },
    {
        $group: {_id: null, max_price: { $max: "$price" }}
    }
    
])



// No. 6

db.fruits.aggregate([
    {
        $match: { "onSale": true}
    },
    {
        $group: {_id: null, min_price: { $min: "$price" }}
    }
    
])

